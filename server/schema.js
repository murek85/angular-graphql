const { gql } = require('apollo-server');

const typeDefs = gql`
  type Station {
    id: ID!
    stationName: String
    gegrLat: Float
    gegrLon: Float
    city: City
    addressStreet: String
  },
  type City {
    id: ID!
    name: String
    commune: Commune
  },
  type Commune {
    communeName: String
    districtName: String
    provinceName: String
  },
  type Query {
    station(cityName: String!): [Station]
    stations: [Station]
  }
`;

module.exports = typeDefs;