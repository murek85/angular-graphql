import { Component, OnInit } from '@angular/core';

import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';

const COUNTRIES_QUERY = gql`
  query {
    countries {
        name
    }
  }
`;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'eapp';
  countries: any[] = [];

  private query: QueryRef<any>;

  constructor(private apollo: Apollo) { }

  ngOnInit(): void {
    this.query = this.apollo.watchQuery({
      query: COUNTRIES_QUERY,
      variables: { }
    });

    this.query.valueChanges.subscribe(result => {
      this.countries = result.data && result.data.countries;
    });
  }
}
