const { ApolloServer } = require('apollo-server');
const { fetch } = require('node-fetch');
const { find, filter } = require('lodash');

const { RESTDataSource } = require('apollo-datasource-rest');

// Import of the file where we build data structures/schemas
const typeDefs = require('./server/schema');

class StationsAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = `http://api.gios.gov.pl/pjp-api/rest`;
  }

  async getStation(cityName) {
    const stations = await this.get(`/station/findAll`);
    const findStations = stations.filter(s => s.city.name === cityName);
    const allData = await Promise.all(findStations)
    return allData;
  }

  async getStations() {
    const stations = await this.get(`/station/findAll`);
    return stations;
  }
}

// Bind queries from schemas definition to data from API's
const resolvers = {
  Query: {
    station: async (_source, { cityName }, { dataSources }) => {
      return dataSources.stationsAPI.getStation(cityName);
    },
    stations: async (_source, { }, { dataSources }) => {
      return dataSources.stationsAPI.getStations();
    }
  }
};

// Build new server
const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      stationsAPI: new StationsAPI()
    };
  }
});

// Start new server
server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});